#ifndef VEC2_H
#define VEC2_H

#include <ostream>

struct vec2{
    float x, y;
    vec2();
    vec2(float r);
    vec2(float x, float y);
    vec2 cross() const;
    float len() const;
};

vec2 operator-(const vec2& v);
vec2 operator+(const vec2& v1, const vec2& v2);
vec2 operator-(const vec2& v1, const vec2& v2);
vec2 operator*(const vec2& v, float n);
vec2 operator*(float n, const vec2& v);
vec2 operator/(const vec2& v, float n);
void operator+=(vec2& v1, const vec2& v2);
void operator-=(vec2& v1, const vec2& v2);
void operator*=(vec2& v, float n);
void operator/=(vec2& v, float n);
float operator*(const vec2& v1, const vec2& v2);
float operator%(const vec2& v1, const vec2& v2);
std::ostream& operator<<(std::ostream& os, const vec2& v);

#endif // vec2_H
