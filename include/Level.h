#ifndef LEVEL_H
#define LEVEL_H

#undef __STRICT_ANSI__
#include <math.h>
#define SFML_STATIC
#include <SFML/Graphics.hpp>
#include <vector>

#include "vec2.h"

class Level{
    sf::RenderWindow& window;
    vec2 mouse, mousePressed, cursor, cursorPressed;
    bool keyLeft, keyRight, keyUp, keyDown, keySpace, keyCtrl, keyShift, keyAlt, lmb, rmb;
    const float aspectRatio;
    struct Bot{
        vec2 pos, posPrev, dest, destNow;
        float r, target;
        bool ground;
        Bot(const vec2& pos): pos(pos), dest(pos), destNow(pos), r(0.01), target(0), ground(false){};
    };
    bool pause;
    std::vector<Bot> bots;
    std::vector<std::vector<int> > connections;
    std::vector<std::vector<bool> > connectionsWanted;
    std::vector<vec2> destinations;
    std::vector<int> matching;
    void setup(int n);
    void updateDestinations();
    void updatePositions(float dt, float speed);
    void removeIndex(std::vector<std::vector<bool> >& removed, std::vector<int>& remainingBot, std::vector<int>& remainingDest, int i, int j);
    bool optimalMatching(const std::vector<std::vector<bool> >& removed, std::vector<int>& matching, int n);
    void zeroMatching(std::vector<std::vector<float> >& d, std::vector<int>& matching);
    bool zeroMatchingAugment(std::vector<int>& matching, const std::vector<std::vector<float> >& d);
    bool connected(const std::vector<Bot>& bots, const std::vector<std::vector<int> >& connections, int a, int b);
    std::vector<std::pair<int, int> > holdMinimumDistance(std::vector<vec2>& v, float d, float w);
    float stayConnected(std::vector<vec2>& v, const std::vector<std::vector<int> >& connections, float d);
    std::pair<std::vector<vec2>, std::vector<std::vector<int> > > testStructure(const vec2& p, float r, int n);
    std::vector<std::vector<float> > graphDistance(const std::vector<Bot>& bots, const std::vector<vec2>& destinations);
    float addBot(const vec2& pos);
    float addDestination(const vec2& pos);
    public:
        Level(sf::RenderWindow& window, float aspectRatio);
        bool input(sf::Event& event);
        bool inputMousePos(const vec2& pos);
        bool update(float dt);
        void draw(float dt);
        static void drawRectangle(const vec2& a, const vec2& b, bool fill);
        static void drawCircle(const vec2& pos, float r, bool fill);
        static void drawLine(const vec2& a, const vec2& b);
};

#endif // LEVEL_H
