#define SFML_STATIC
#include <SFML/Graphics.hpp>
#define GLEW_STATIC
#include <GL/glew.h>
#include <iostream>

#include "Level.h"

using namespace std;

int main(int argc, char* argv[]){
    sf::RenderWindow window(sf::VideoMode::getDesktopMode(),"Paint Wars",sf::Style::Fullscreen,sf::ContextSettings(24,8,8));
    window.setVerticalSyncEnabled(true);
    glewInit();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_MULTISAMPLE);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float aspectRatio = (float)window.getSize().x/window.getSize().y;
    glOrtho(0,aspectRatio,0,1,-1,0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    Level level(window,aspectRatio);
    sf::Event event;
    sf::Clock cl;
    float time = cl.getElapsedTime().asSeconds();
    int c = 0;
    while(true){
        ++c;
        if(c == 60){
            static float fpsTime = cl.getElapsedTime().asSeconds();
            float t = cl.getElapsedTime().asSeconds();
            float fps = c/(t-fpsTime);
            fpsTime = t;
            c = 0;
            cout << "fps: " << fps << endl;
        }
        bool inp = true;
        while(window.pollEvent(event)){
            if(!level.input(event)){
                inp = false;
                break;
            }
        }
        if(!inp) break;
        sf::Vector2i m = sf::Mouse::getPosition(window);
        if(!level.inputMousePos(vec2((float)m.x/window.getSize().y,1-(float)m.y/window.getSize().y))) break;
        float t = cl.getElapsedTime().asSeconds();
        //float dt = fmin(t-time,1./30);
        float dt = 1./60;
        time = t;
        if(!level.update(dt)) break;
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        level.draw(dt);
        window.display();
    }
    window.close();
    return 0;
}
