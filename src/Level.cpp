#include "Level.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glu.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <queue>

using namespace std;

Level::Level(sf::RenderWindow& window, float aspectRatio):
        window(window),
        aspectRatio(aspectRatio), pause(true){
    keyUp = keyDown = keyLeft = keyRight = keySpace = keyCtrl = keyShift = keyAlt = lmb = rmb = false;
    /*for(int i = 1; i <= 30; i += 1){
        sf::Clock cl;
        float t1 = cl.getElapsedTime().asSeconds();
        for(int j = 0; j < 1000; ++j) setup(i);
        float t2 = cl.getElapsedTime().asSeconds();
        cout << t2-t1 << ',';
    }*/
}

float Level::addBot(const vec2& pos){
    int minI = -1;
    float minDist = pos.y+Bot(vec2(0,0)).r;
    for(int i = 0; i < bots.size(); ++i){
        vec2 d = bots[i].pos-pos;
        float dSqr = d*d;
        if(dSqr < minDist*minDist){
            minI = i;
            minDist = sqrt(dSqr);
        }
    }
    if(minDist < /*2**/0.5*Bot(vec2(0,0)).r) return 0;
    bots.push_back(Bot(pos));
    connections.push_back(vector<int>());
    connectionsWanted = vector<vector<bool> >(bots.size(), vector<bool>(bots.size(), false));
    vec2 d;
    if(minI == -1){
        d = vec2(0, -1);
        bots[bots.size()-1].pos.y = Bot(vec2(0,0)).r;
        bots[bots.size()-1].ground = true;
    }
    else{
        connections[bots.size()-1].push_back(minI);
        connections[minI].push_back(bots.size()-1);
        d = bots[minI].pos-pos;
        d /= sqrt(d*d);
        bots[bots.size()-1].pos = bots[minI].pos-d*bots[minI].r*2;
    }
    matching.clear();

    for(int iter = 0; iter < 10; ++iter) for(int i = 0; i < bots.size(); ++i){
        bots[i].pos.y = fmax(bots[i].pos.y, Bot(vec2(0,0)).r);
        for(int j = i+1; j < bots.size(); ++j){
            vec2 d = bots[j].pos-bots[i].pos;
            float dSqr = d*d;
            float r = 2*Bot(vec2(0,0)).r;
            if(dSqr > r*r) continue;
            float dLen = sqrt(dSqr);
            d *= (dLen-r)/dLen;
            bots[i].pos += d/2;
            bots[j].pos -= d/2;
        }
    }
    return minDist;
}

float Level::addDestination(const vec2& pos){
    int minI = -1;
    float minDist = pos.y+Bot(vec2(0,0)).r;
    for(int i = 0; i < destinations.size(); ++i){
        vec2 d = destinations[i]-pos;
        float dSqr = d*d;
        if(dSqr < minDist*minDist){
            minI = i;
            minDist = sqrt(dSqr);
        }
    }
    if(minDist < /*2**/0.5*Bot(vec2(0,0)).r) return 0;
    destinations.push_back(pos);
    if(minI == -1) destinations[destinations.size()-1].y = Bot(vec2(0,0)).r;
    else{
        vec2 d = destinations[minI]-pos;
        d /= sqrt(d*d);
        destinations[destinations.size()-1] = destinations[minI]-d*Bot(vec2(0,0)).r*2;
    }
    matching.clear();

    for(int iter = 0; iter < 10; ++iter) for(int i = 0; i < destinations.size(); ++i){
        destinations[i].y = fmax(destinations[i].y, Bot(vec2(0,0)).r);
        for(int j = i+1; j < destinations.size(); ++j){
            vec2 d = destinations[j]-destinations[i];
            float dSqr = d*d;
            float r = 2*Bot(vec2(0,0)).r;
            if(dSqr > r*r) continue;
            float dLen = sqrt(dSqr);
            d *= (dLen-r)/dLen;
            destinations[i] += d/2;
            destinations[j] -= d/2;
        }
    }
    return minDist;
}

bool Level::input(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            return false;
        case sf::Event::KeyPressed:
        case sf::Event::KeyReleased:{
            bool down = event.type == sf::Event::KeyPressed;
            switch(event.key.code){
                case sf::Keyboard::Escape:
                    if(down) return false;
                    break;
                case sf::Keyboard::Up:
                    keyUp = down;
                    //if(down) setup(bots.size()+1+9*keyCtrl);
                    break;
                case sf::Keyboard::Down:
                    keyDown = down;
                    //if(down) setup(max((int)bots.size()-1-9*keyCtrl, 1));
                    break;
                case sf::Keyboard::Left:
                    keyLeft = down;
                    break;
                case sf::Keyboard::Right:
                    keyRight = down;
                    if(down){
                        pause = false;
                        update(0.0001);
                        pause = true;
                    }
                    break;
                case sf::Keyboard::Space:
                    keySpace = down;
                    if(down) pause = !pause;
                    if(!pause) updateDestinations();
                    break;
                case sf::Keyboard::Return:
                    //if(down) setup(bots.size());
                    break;
                case sf::Keyboard::LControl:
                    keyCtrl = down;
                    break;
                case sf::Keyboard::LShift:
                    keyShift = down;
                    break;
                case sf::Keyboard::LAlt:
                    keyAlt = down;
                    break;
                case sf::Keyboard::Q:
                    if(down){
                        destinations.clear();
                        pause = true;
                    }
                    break;
            }
            break;
        }
        case sf::Event::MouseButtonPressed:
        case sf::Event::MouseButtonReleased:{
            bool down = event.type == sf::Event::MouseButtonPressed;
            switch(event.mouseButton.button){
                case sf::Mouse::Left:{
                    lmb = down;
                    if(!down) break;
                    cout << mouse << endl;
                    if(keyCtrl) while(addBot(mouse) > 2*Bot(vec2(0,0)).r);
                    else if(keyShift) while(addBot(mouse) > 2*Bot(vec2(0,0)).r && bots.size() < destinations.size());
                    else addBot(mouse);
                    break;}
                case sf::Mouse::Right:
                    rmb = down;
                    if(!down) break;
                    cout << mouse << endl;
                    if(keyCtrl) while(addDestination(mouse) > 2*Bot(vec2(0,0)).r);
                    else if(keyShift) while(addDestination(mouse) > 2*Bot(vec2(0,0)).r && destinations.size() < bots.size());
                    else addDestination(mouse);
                    break;
            }
        }
    }
    return true;
}

bool Level::inputMousePos(const vec2& pos){
    mouse = pos;
    return true;
}

bool Level::update(float dt){
    if(keyAlt && (lmb || rmb)){
        float radius = Bot(vec2(0,0)).r;
        vec2 pos = mouse+vec2((float)rand()/RAND_MAX-0.5, (float)rand()/RAND_MAX-0.5)*2*radius;
        if(rmb) addDestination(pos);
        else if(bots.size() < destinations.size()) addBot(pos);
        pause = true;
        return true;
    }
    if(pause) return true;
    if(bots.size() != destinations.size()){
        cout << bots.size() << " is not " << destinations.size() << endl;
        pause = true;
        return true;
    }
    float speed = 0.1;
    float dtMax = 0.5*Bot(vec2(0,0)).r/speed;
    while(dt > dtMax){
        if(!update(dtMax)) return false;
        dt -= dtMax;
    }
    if((float)rand()/RAND_MAX > pow(0.3, speed*dt)) updateDestinations();
    for(int i = 0; i < bots.size(); ++i){
        bots[i].posPrev = bots[i].pos;
    }
    /*for(int i = 0; i < bots.size(); ++i){
        bool fall = true;
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            if(bots[j].pos.y < bots[i].pos.y){
                fall = false;
                break;
            }
        }
        if(fall) bots[i].pos.y -= speed*dt;
    }*/
    for(int i = 0; i < 20; ++i){
        updatePositions(dt, speed);
    }
    return true;
}

void Level::draw(float dt){
    glColor3f(0,0,1);
    for(int i = 0; i < bots.size(); ++i){
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            drawLine(bots[i].pos, bots[j].pos);
        }
    }
    glColor3f(1,0,1);
    for(int i = 0; i < bots.size(); ++i){
        for(int j = 0; j < bots.size(); ++j){
            if(connectionsWanted[i][j]) drawLine(bots[i].pos, bots[j].pos);
        }
    }
    glColor3f(1,1,0);
    for(int i = 0; i < bots.size(); ++i){
        drawLine(bots[i].pos, bots[i].destNow);
    }
    glColor3f(1,0,0);
    for(int i = 0; i < destinations.size(); ++i){
        glPushMatrix();
        glTranslatef(destinations[i].x, destinations[i].y, 0);
        float r = Bot(vec2(0,0)).r*0.5;
        glScalef(r, r, 1);
        drawRectangle({-1,-1}, {1,1}, true);
        glPopMatrix();
    }
    for(int i = 0; i < bots.size(); ++i){
        glColor3f(bots[i].target, 1, bots[i].target);
        drawCircle(bots[i].pos, bots[i].r, true);
    }
}

void Level::drawRectangle(const vec2& a, const vec2& b, bool fill){
    glBegin(fill ? GL_QUADS : GL_LINE_STRIP);
    glVertex3f(a.x, a.y, .85);
    glVertex3f(b.x, a.y, .85);
    glVertex3f(b.x, b.y, .85);
    glVertex3f(a.x, b.y, .85);
    glVertex3f(a.x, a.y, .85);
    glEnd();
}

void Level::drawCircle(const vec2& pos, float r, bool fill){
    glPushMatrix();
    glTranslatef(pos.x, pos.y, 0);
    glScalef(r, r, 1);
    glBegin(fill ? GL_TRIANGLE_FAN : GL_LINE_STRIP);
    for(float a = 0; a < 2*M_PI; a += M_PI_4*0.2){
        glVertex3f(cos(a), sin(a), 0.5);
    }
    glEnd();
    glPopMatrix();
}

void Level::drawLine(const vec2& a, const vec2& b){
    glBegin(GL_LINES);
    glVertex3f(a.x, a.y, 0.5);
    glVertex3f(b.x, b.y, 0.5);
    glEnd();
}

void Level::setup(int n){
    //cout << "n = " << n << endl;
    bots.clear();
    connections.clear();
    connectionsWanted.clear();
    destinations.clear();
    matching.clear();
    pair<vector<vec2>, vector<vector<int> > > structure = testStructure(vec2(0.5, 0.5), Bot(vec2(0,0)).r*2, n);
    vector<vec2> pos = structure.first;
    connections = structure.second;
    connectionsWanted = vector<vector<bool> >(connections.size(), vector<bool>(connections.size(), false));
    for(int i = 0; i < pos.size(); ++i){
        bots.push_back(Bot(pos[i]));
    }
    destinations = testStructure(vec2(aspectRatio-0.5, 0.5), Bot(vec2(0,0)).r*2, n).first;
    /*for(int i = 0; destinations.size() < n; ++i){
        for(int j = 0; j*j < n && destinations.size() < n; ++j){
            destinations.push_back(vec2(1.5,0.5)+vec2(i*bots[0].r*2,j*bots[0].r*2));
        }
    }*/
    updateDestinations();
}

void Level::updateDestinations(){
    if(bots.size() != destinations.size()){
        cout << bots.size() << " is not " << destinations.size() << endl;
        return;
    }
    vector<vector<float> > dist(bots.size(), vector<float>(bots.size()));
    for(int i = 0; i < bots.size(); ++i){
        for(int j = 0; j < bots.size(); ++j){
            vec2 d = destinations[j]-bots[i].pos;
            dist[i][j] = sqrt(d*d);
        }
    }
    //vector<vector<float> > dist = graphDistance(bots, destinations);
    for(int i = 0; i < dist.size(); ++i){
        for(int j = 0; j < dist.size(); ++j){
            dist[i][j] *= dist[i][j];
        }
    }
    static vector<vector<float> > diff;
    if(diff.size() != dist.size()) diff = vector<vector<float> >(dist.size(), vector<float>(dist.size(), 0));
    vector<vector<float> > distReduced = dist;
    float minimum = 0;
    for(int i = 0; i < dist.size(); ++i){
        for(int j = 0; j < dist.size(); ++j){
            distReduced[i][j] -= diff[i][j];
            minimum = fmin(minimum, distReduced[i][j]);
        }
    }
    for(int i = 0; i < dist.size(); ++i){
        for(int j = 0; j < dist.size(); ++j){
            distReduced[i][j] -= minimum;
        }
    }
    zeroMatching(distReduced, matching);
    for(int i = 0; i < dist.size(); ++i){
        for(int j = 0; j < dist.size(); ++j){
            diff[i][j] = dist[i][j]-distReduced[i][j];
        }
    }
    for(int j = 0; j < matching.size(); ++j){
        bots[matching[j]].dest = destinations[j];
    }
    /*vector<vector<bool> > removed(bots.size(), vector<bool>(bots.size(), false));
    vector<int> remainingBot(bots.size(), bots.size());
    vector<int> remainingDest(bots.size(), bots.size());
    vector<int> matching(bots.size());
    priority_queue<pair<float, pair<int, int> > > check;
    for(int i = 0; i < bots.size(); ++i){
        bots[i].dest = {0,0};
        matching[i] = i;
        for(int j = 0; j < bots.size(); ++j){
            vec2 d = destinations[j]-bots[i].pos;
            check.push(make_pair(d*d, make_pair(i, j)));
        }
    }
    while(!check.empty()){
        int i = check.top().second.first;
        int j = check.top().second.second;
        check.pop();
        if(removed[i][j]) continue;
        removed[i][j] = true;
        --remainingBot[i];
        --remainingDest[j];
        if(matching[j] != i) continue;
        if(optimalMatching(removed, matching, i)) continue;
        bots[i].dest = destinations[j];
        removeIndex(removed, remainingBot, remainingDest, i, j);
    }*/
}

void Level::updatePositions(float dt, float speed){
    // Free movement
    vector<int> count(bots.size(), 1);
    for(int i = 0; i < bots.size(); ++i){
        bots[i].destNow = bots[i].pos;
    }
    for(int i = 0; i < bots.size(); ++i){
        if(bots[i].dest.y < bots[i].r+0.0001){
            bots[i].destNow += bots[i].pos;
            bots[i].destNow.y -= bots[i].r;
            ++count[i];
        }
        for(int j = i+1; j < bots.size(); ++j){
            vec2 d = bots[j].dest-bots[i].dest;
            float dSqr = d*d;
            float r = bots[i].r+bots[j].r;
            connectionsWanted[i][j] = connectionsWanted[j][i] = false;
            if(dSqr > r*r*1.01) continue;
            connectionsWanted[i][j] = connectionsWanted[j][i] = true;
            d = bots[j].pos-bots[i].pos;
            dSqr = d*d;
            if(dSqr < r*r*1.01) continue;
            bots[i].destNow += bots[j].pos;
            bots[j].destNow += bots[i].pos;
            ++count[i];
            ++count[j];
        }
    }
    for(int i = 0; i < bots.size(); ++i){
        bots[i].destNow /= count[i];
        bots[i].destNow = bots[i].destNow*(1-bots[i].target)+bots[i].dest*bots[i].target;
    }
    float maxVel = 0;
    for(int i = 0; i < bots.size(); ++i){
        vec2 vel = bots[i].destNow-bots[i].pos;
        float velLen = sqrt(vel*vel);
        if(velLen < 0.00000001) continue;
        maxVel = fmax(maxVel, sqrt(vel*vel));
    }
    if(maxVel < 0.000001) maxVel = 1;
    for(int i = 0; i < bots.size(); ++i){
        vec2 vel = bots[i].destNow-bots[i].pos;
        float velLen = sqrt(vel*vel);
        if(velLen < 0.00000001) continue;
        vel /= velLen;
        float velLenNew = 0.5*Bot(vec2(0,0)).r*velLen/maxVel;
        vec2 d = bots[i].destNow-bots[i].pos;
        velLenNew = fmin(velLenNew, sqrt(d*d));
        vel *= velLenNew;
        bots[i].pos += vel;
    }
    vector<vector<bool> > connectionsMatrix(bots.size(), vector<bool>(bots.size(), false));
    for(int i = 0; i < bots.size(); ++i){
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            connectionsMatrix[i][j] = true;
        }
    }
    bool allConnected = true;
    for(int i = 0; i < bots.size(); ++i){
        if(bots[i].ground) continue;
        if(bots[i].dest.y > bots[i].r+0.0001) continue;
        bots[i].target *= pow(0.5, dt);
        allConnected = false;
    }
    for(int i = 0; i < connectionsMatrix.size(); ++i){
        for(int j = i+1; j < connectionsMatrix.size(); ++j){
            if(connectionsMatrix[i][j]) continue;
            if(!connectionsWanted[i][j]) continue;
            bots[i].target *= pow(0.5, dt);
            bots[j].target *= pow(0.5, dt);
            allConnected = false;
        }
    }
    if(allConnected) for(int i = 0; i < bots.size(); ++i){
        bots[i].destNow = bots[i].pos;
        --bots[i].target;
        bots[i].target *= pow(0.99, dt);
        ++bots[i].target;
    }
    for(int i = 0; i < bots.size(); ++i){
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            float target = fmin(bots[i].target, bots[j].target);
            bots[i].target -= target;
            bots[i].target *= 0;//pow(0.5, dt);
            bots[i].target += target;
            bots[j].target -= target;
            bots[j].target *= 0;//pow(0.5, dt);
            bots[j].target += target;
        }
    }
    // Constrained movement
    for(int i = 0; i < bots.size(); ++i){
        if(bots[i].pos.y < bots[i].r){
            bots[i].ground = true;
            bots[i].pos.y = bots[i].r;
        }
        for(int j = i+1; j < bots.size(); ++j){
            vec2 d = bots[j].pos-bots[i].pos;
            float dSqr = d*d;
            float r = bots[i].r+bots[j].r;
            if(dSqr > r*r*1.01) continue;
            bool connected = false;
            for(int c = 0; c < connections[i].size(); ++c){
                if(connections[i][c] == j){
                    connected = true;
                    break;
                }
            }
            if(connected) continue;
            connections[i].push_back(j);
            connections[j].push_back(i);
            if(dSqr > r*r) continue;
            float dLen = sqrt(dSqr);
            d *= (dLen-r)/dLen;
            bots[i].pos += d/2;
            bots[j].pos -= d/2;
        }
    }
    // Remove unwanted connections
    priority_queue<pair<float, pair<int, int> > > check;
    for(int i = 0; i < bots.size(); ++i){
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            if(connectionsWanted[i][j]) continue;
            vec2 d = bots[j].dest-bots[i].dest;
            float r = bots[i].r+bots[j].r;
            check.push(make_pair(d*d,make_pair(i, j)));
        }
    }
    while(!check.empty()){
        int i = check.top().second.first;
        int j = check.top().second.second;
        check.pop();
        if(!connected(bots, connections, i, j)) continue;
        for(int c = 0; c < connections[i].size(); ++c){
            if(connections[i][c] != j) continue;
            connections[i].erase(connections[i].begin()+c);
            break;
        }
        for(int c = 0; c < connections[j].size(); ++c){
            if(connections[j][c] != i) continue;
            connections[j].erase(connections[j].begin()+c);
            break;
        }
    }
    for(int i = 0; i < bots.size(); ++i){
        if(!bots[i].ground) continue;
        if(bots[i].dest.y < bots[i].r+0.0001) continue;
        bots[i].ground = false;
        if(connected(bots, connections, i, i)) continue;
        bots[i].ground = true;
    }
    // Correct according to connections
    for(int iter = 0; iter < 4; ++iter){
        for(int i = 0; i < bots.size(); ++i){
            if(bots[i].ground) bots[i].pos.y = bots[i].r;
            for(int c = 0; c < connections[i].size(); ++c){
                int j = connections[i][c];
                vec2 d = bots[j].pos-bots[i].pos;
                float dSqr = d*d;
                float r = bots[i].r+bots[j].r;
                float dLen = sqrt(dSqr);
                d *= 0.99*(r-dLen)/dLen;
                bots[i].pos -= 0.5*d;
                bots[j].pos += 0.5*d;
            }
        }
        // Correct according to max speed
        for(int i = 0; i < bots.size(); ++i){
            vec2 d = bots[i].pos-bots[i].posPrev;
            float dSqr = d*d;
            float r = speed*dt;
            if(dSqr < r*r) continue;
            d *= r/sqrt(dSqr);
            bots[i].pos = bots[i].posPrev+d;
        }
    }
}

void Level::removeIndex(vector<vector<bool> >& removed, vector<int>& remainingBot, vector<int>& remainingDest, int i, int j){
    for(int a = 0; a < bots.size(); ++a){
        if(removed[a][j]) continue;
        removed[a][j] = true;
        --remainingBot[a];
        --remainingDest[j];
    }
    for(int b = 0; b < bots.size(); ++b){
        if(removed[i][b]) continue;
        removed[i][b] = true;
        --remainingBot[i];
        --remainingDest[b];
    }
}

bool Level::optimalMatching(const vector<vector<bool> >& removed, std::vector<int>& matching, int n){
    vector<pair<int, int> > traceBack(matching.size(), make_pair(-1, -1));
    queue<int> check;
    check.push(n);
    while(!check.empty()){
        int i = check.front();
        check.pop();
        for(int j = 0; j < traceBack.size(); ++j){
            if(removed[i][j]) continue;
            if(matching[j] == i) continue;
            if(traceBack[matching[j]].first != -1) continue;
            traceBack[matching[j]].first = j;
            traceBack[matching[j]].second = i;
            check.push(matching[j]);
        }
    }
    if(traceBack[n].first == -1) return false;
    matching[traceBack[n].first] = traceBack[n].second;
    for(int i = traceBack[n].second; i != n; i = traceBack[i].second){
        matching[traceBack[i].first] = traceBack[i].second;
    }
    return true;
}

void Level::zeroMatching(vector<vector<float> >& d, vector<int>& matching){
    if(matching.size() == 0){
        matching = vector<int>(d.size());
        for(int j = 0; j < matching.size(); ++j){
            matching[j] = j;
        }
    }
    for(int i = 0; i < d.size(); ++i){
        float minimum = d[i][0];
        for(int j = 1; j < d.size(); ++j){
            minimum = fmin(minimum, d[i][j]);
        }
        for(int j = 0; j < d.size(); ++j){
            d[i][j] -= minimum;
        }
    }
    for(int j = 0; j < d.size(); ++j){
        float minimum = d[0][j];
        for(int i = 1; i < d.size(); ++i){
            minimum = fmin(minimum, d[i][j]);
        }
        for(int i = 0; i < d.size(); ++i){
            d[i][j] -= minimum;
        }
    }
    int matchings = 0;
    for(int j = 0; j < matching.size(); ++j){
        if(d[matching[j]][j] < 0.0001) ++matchings;
    }
    while(matchings < matching.size()){
        while(zeroMatchingAugment(matching, d)) ++matchings;
        vector<bool> increaseRow(matching.size(), false);
        vector<bool> increaseCol(matching.size(), false);
        int ir = 0;
        for(int j = 0; j < matching.size(); ++j){
            int i = matching[j];
            if(d[i][j] < 0.0001) continue;
            for(int a = 0; a < matching.size(); ++a){
                if(d[a][j] >= 0.0001) continue;
                increaseRow[a] = true;
                ++ir;
            }
            for(int b = 0; b < matching.size(); ++b){
                if(d[i][b] >= 0.0001) continue;
                increaseCol[b] = true;
                --ir;
            }
        }
        vector<int> matchingInv(matching.size());
        for(int i = 0; i < matching.size(); ++i){
            for(int j = 0; j < matching.size(); ++j){
                if(matching[j] == i) matchingInv[i] = j;
            }
        }
        queue<int> check;
        for(int j = 0; j < matching.size(); ++j){
            int i = matching[j];
            check.push(j);
        }
        while(!check.empty()){
            int j = check.front();
            check.pop();
            int i = matching[j];
            if(d[i][j] >= 0.0001) continue;
            if(!increaseRow[i]) continue;
            for(int a = 0; a < matching.size(); ++a){
                if(increaseRow[a]) continue;
                if(d[a][j] >= 0.0001) continue;
                increaseRow[a] = true;
                check.push(a);
            }
        }
        for(int j = 0; j < matching.size(); ++j){
            int i = matching[j];
            check.push(i);
        }
        while(!check.empty()){
            int i = check.front();
            check.pop();
            int j = matchingInv[i];
            if(d[i][j] >= 0.0001) continue;
            if(!increaseCol[j]) continue;
            for(int b = 0; b < matching.size(); ++b){
                if(increaseCol[b]) continue;
                if(d[i][b] >= 0.0001) continue;
                increaseCol[b] = true;
                check.push(b);
            }
        }
        for(int i = 0; i < matching.size(); ++i){
            if(increaseRow[i]) continue;
            for(int j = 0; j < matching.size(); ++j){
                if(increaseCol[j]) continue;
                if(d[i][j] >= 0.0001) continue;
                increaseRow[i] = true;
            }
        }
        vector<vector<int> > increase(matching.size(), vector<int>(matching.size(), -1));
        for(int a = 0; a < increaseRow.size(); ++a){
            if(!increaseRow[a]) continue;
            for(int b = 0; b < matching.size(); ++b){
                ++increase[a][b];
            }
        }
        for(int b = 0; b < increaseCol.size(); ++b){
            if(!increaseCol[b]) continue;
            for(int a = 0; a < matching.size(); ++a){
                ++increase[a][b];
            }
        }
        float minimum = -1;
        for(int i = 0; i < increase.size(); ++i){
            for(int j = 0; j < increase.size(); ++j){
                if(increase[i][j] != -1) continue;
                if(minimum < 0 || d[i][j] < minimum) minimum = d[i][j];
            }
        }
        for(int i = 0; i < increase.size(); ++i){
            for(int j = 0; j < increase.size(); ++j){
                d[i][j] += minimum*increase[i][j];
            }
        }
        matchings = 0;
        for(int j = 0; j < matching.size(); ++j){
            if(d[matching[j]][j] < 0.0001) ++matchings;
        }
    }
}

bool Level::zeroMatchingAugment(vector<int>& matching, const vector<vector<float> >& d){
    vector<int> matchingInv(matching.size());
    for(int i = 0; i < matching.size(); ++i){
        for(int j = 0; j < matching.size(); ++j){
            if(matching[j] == i) matchingInv[i] = j;
        }
    }
    vector<pair<int, int> > traceBack(matching.size(), make_pair(-1, -1));
    queue<int> check;
    for(int j = 0; j < matching.size(); ++j){
        if(d[matching[j]][j] < 0.0001) continue;
        check.push(matching[j]);
    }
    while(!check.empty()){
        int i = check.front();
        check.pop();
        for(int j = 0; j < traceBack.size(); ++j){
            if(matching[j] == i) continue;
            if(d[i][j] >= 0.0001) continue;
            int i2 = matching[j];
            if(traceBack[i2].first != -1) continue;
            traceBack[i2].first = j;
            traceBack[i2].second = i;
            if(d[i2][j] >= 0.0001){
                int inv = matchingInv[traceBack[i2].second];
                matching[traceBack[i2].first] = traceBack[i2].second;
                matchingInv[traceBack[i2].second] = traceBack[i2].first;
                int a;
                for(a = traceBack[i2].second; traceBack[a].first != -1 && a != i2; a = traceBack[a].second){
                    inv = matchingInv[traceBack[a].second];
                    matching[traceBack[a].first] = traceBack[a].second;
                    matchingInv[traceBack[a].second] = traceBack[a].first;
                }
                if(matching[inv] == a) matching[inv] = i2;
                return true;
            }
            check.push(i2);
        }
    }
    return false;
}

bool Level::connected(const vector<Bot>& bots, const vector<vector<int> >& connections, int a, int b){
    vector<bool> checkedA(connections.size(), false), checkedB(connections.size(), false);
    queue<int> checkA, checkB;
    checkA.push(a);
    checkB.push(b);
    while(!checkA.empty()){
        int i = checkA.front();
        if(bots[i].ground) break;
        checkA.pop();
        if(checkedA[i]) continue;
        checkedA[i] = true;
        for(int c = 0; c < connections[i].size(); ++c){
            if(i == a && connections[i][c] == b) continue;
            checkA.push(connections[i][c]);
        }
    }
    if(checkA.empty()) return false;
    while(!checkB.empty()){
        int i = checkB.front();
        if(bots[i].ground) break;
        checkB.pop();
        if(checkedB[i]) continue;
        checkedB[i] = true;
        for(int c = 0; c < connections[i].size(); ++c){
            if(i == b && connections[i][c] == a) continue;
            checkB.push(connections[i][c]);
        }
    }
    if(checkB.empty()) return false;
    return true;
}

vector<pair<int, int> > Level::holdMinimumDistance(std::vector<vec2>& v, float d, float w){
    vector<pair<int, int> > touching;
    for(int i = 0; i < v.size(); ++i){
        for(int j = i+1; j < v.size(); ++j){
            vec2 dist = v[j]-v[i];
            float distSqr = dist*dist;
            if(distSqr > d*d) continue;
            if(distSqr < 0.0001) continue;
            float distLen = sqrt(distSqr);
            dist *= 0.5*w*(distLen-d)/distLen;
            v[i] += dist;
            v[j] -= dist;
            touching.push_back(make_pair(i, j));
        }
    }
    return touching;
}

float Level::stayConnected(std::vector<vec2>& v, const std::vector<std::vector<int> >& connections, float d){
    float totalDistance = 0;
    for(int i = 0; i < v.size(); ++i){
        for(int c = 0; c < connections[i].size(); ++c){
            int j = connections[i][c];
            vec2 dist = v[j]-v[i];
            float distSqr = dist*dist;
            float distLen = sqrt(distSqr);
            float distance = distLen-d;
            dist *= 0.5*0.99*distance/distLen;
            v[i] += dist;
            v[j] -= dist;
            totalDistance += distance;
        }
    }
    return totalDistance;
}

pair<vector<vec2>, vector<vector<int> > > Level::testStructure(const vec2& p, float r, int n){
    vector<vec2> pos;
    vector<vector<int> > connections;
    pos.push_back(p);
    connections.push_back(vector<int>());
    while(pos.size() < n){
        for(int i = 0; i < pos.size() && pos.size() < n; ++i){
            vec2 newPos = pos[i]+vec2(r);
            int j;
            for(j = 0; j < pos.size(); ++j){
                vec2 d = pos[j]-newPos;
                if(d*d < r*r) break;
            }
            if(j < pos.size()) continue;
            pos.push_back(newPos);
            connections.push_back(vector<int>());
            connections[i].push_back(pos.size()-1);
            connections[pos.size()-1].push_back(i);
        }
    }
    /*for(int i = 0; i < pos.size(); ++i){
        pos[i] = p+vec2(r);
        if(i == 0) continue;
        connections[i].push_back(i-1);
        connections[i-1].push_back(i);
    }
    for(int iter = 0; iter < 1000; ++iter){
        float d = Bot(vec2(0,0)).r*2;
        vector<pair<int, int> > newConnections = holdMinimumDistance(pos, d, 0.5);
        for(int a = 0; a < newConnections.size(); ++a){
            int i = newConnections[a].first;
            int j = newConnections[a].second;
            int b;
            for(b = 0; b < connections[i].size(); ++b){
                if(connections[i][b] == j) break;
            }
            if(b < connections[i].size()) continue;
            connections[i].push_back(j);
            connections[j].push_back(i);
        }
        priority_queue<pair<float, pair<int, int> > > check;
        for(int i = 0; i < pos.size(); ++i){
            for(int c = 0; c < connections[i].size(); ++c){
                int j = connections[i][c];
                vec2 dist = pos[j]-pos[i];
                check.push(make_pair(dist*dist,make_pair(i, j)));
            }
        }
        while(!check.empty()){
            int i = check.top().second.first;
            int j = check.top().second.second;
            check.pop();
            if(!connected(connections, i, j)) continue;
            for(int c = 0; c < connections[i].size(); ++c){
                if(connections[i][c] != j) continue;
                connections[i].erase(connections[i].begin()+c);
                break;
            }
            for(int c = 0; c < connections[j].size(); ++c){
                if(connections[j][c] != i) continue;
                connections[j].erase(connections[j].begin()+c);
                break;
            }
        }
        stayConnected(pos, connections, d);
    }*/
    return make_pair(pos, connections);
}

vector<vector<float> > Level::graphDistance(const vector<Bot>& bots, const vector<vec2>& destinations){
    float r = bots[0].r*2;
    struct Node{
        bool bot;
        int index;
        vec2 pos;
        vector<int> connections;
        bool checked;
    };
    vector<Node> graph(bots.size()*2);
    for(int i = 0; i < bots.size(); ++i){
        graph[i].bot = true;
        graph[i].index = i;
        graph[i].pos = bots[i].pos;
        graph[bots.size()+i].bot = false;
        graph[bots.size()+i].index = i;
        graph[bots.size()+i].pos = destinations[i];
    }
    int closestIndexI = -1;
    int closestIndexJ = -1;
    float closestDistance;
    bool connected = false;
    for(int i = 0; i < graph.size(); ++i){
        for(int j = i+1; j < graph.size(); ++j){
            vec2 d = graph[j].pos-graph[i].pos;
            float dSqr = d*d;
            if(i < bots.size() && j >= bots.size() && (closestIndexI == -1 || dSqr < closestDistance)){
                closestIndexI = i;
                closestIndexJ = j;
                closestDistance = dSqr;
            }
            if(dSqr > r*r*1.01) continue;
            graph[i].connections.push_back(j);
            graph[j].connections.push_back(i);
            if(i < bots.size() && j >= bots.size()) connected = true;
        }
    }
    if(!connected){
        graph[closestIndexI].connections.push_back(closestIndexJ);
        graph[closestIndexJ].connections.push_back(closestIndexI);
    }
    vector<vector<float> > distance(bots.size(), vector<float>(bots.size(), 0));
    for(int i = 0; i < bots.size(); ++i){
        for(int j = 0; j < graph.size(); ++j){
            graph[j].checked = false;
        }
        priority_queue<pair<float, int> > check;
        check.push(make_pair(0, i));
        while(!check.empty()){
            float d = -check.top().first;
            float j = check.top().second;
            check.pop();
            if(graph[j].checked) continue;
            graph[j].checked = true;
            if(!graph[j].bot) distance[i][j-bots.size()] = d;
            for(int c = 0; c < graph[j].connections.size(); ++c){
                int k = graph[j].connections[c];
                vec2 dist = graph[k].pos-graph[j].pos;
                check.push(make_pair(-d-sqrt(dist*dist), k));
            }
        }
    }
    return distance;
}
